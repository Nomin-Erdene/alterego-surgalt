import { useRouter } from "next/router";
import Head from "next/head";

export default function Article() {
    const router = useRouter();
    const { id } = router.query;

    return (
        <div>
            <Head>
                <title>My page title</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            Article Page {id}
        </div>
    );
}
