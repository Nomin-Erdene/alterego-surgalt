const Category = require("../../mongoose/models/Category");

async function categories(parent, args) {
    const { status } = args;
    const list = await Category.find(status ? { status } : undefined);
    return list;
}

module.exports = categories;
