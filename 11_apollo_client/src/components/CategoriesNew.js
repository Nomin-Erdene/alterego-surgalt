import { useState } from "react";
import { toast } from "react-toastify";
import { gql, useMutation } from "@apollo/client";

const ADD_CATEGORY = gql`
    mutation AddCategory($name: String!, $slug: String) {
        addCategory(name: $name, slug: $slug) {
            _id
            name
            slug
            status
        }
    }
`;

export default function CategoriesNew({ onClose, onComplete }) {
    const [addCategory, { loading }] = useMutation(ADD_CATEGORY, {
        onCompleted() {
            onComplete();
            toast.success("Амжилттай бүртгэлээ");
        },
    });

    const [name, setName] = useState("");
    const [slug, setSlug] = useState("");

    function submit() {
        addCategory({
            variables: {
                name,
                slug,
            },
        });
    }

    return (
        <div className="modal is-active">
            <div className="modal-background"></div>
            <div className="modal-content" style={{ maxWidth: 400 }}>
                <div className="box">
                    <h2 className="title is-3 has-text-centered">Ангилал нэмэх</h2>

                    <div className="field">
                        <label className="label">Нэр</label>
                        <div className="control">
                            <input disabled={loading} value={name} onChange={(e) => setName(e.target.value)} className="input" type="text" placeholder="Ангиллын нэр..." />
                        </div>
                    </div>

                    <div className="field">
                        <label className="label">Холбооc</label>
                        <div className="control">
                            <input disabled={loading} value={slug} onChange={(e) => setSlug(e.target.value)} className="input" type="text" placeholder="Холбооc..." />
                        </div>
                    </div>

                    <hr />

                    <div className="buttons">
                        <button disabled={loading} className="button" onClick={onClose} style={{ flex: 1 }}>
                            Болих
                        </button>
                        <button disabled={loading} className={`button is-link ${loading ? "is-loading" : ""}`} style={{ flex: 1 }} onClick={submit}>
                            Хадгалах
                        </button>
                    </div>
                </div>
            </div>
            <button disabled={loading} className="modal-close is-large" aria-label="close" onClick={onClose}></button>
        </div>
    );
}
