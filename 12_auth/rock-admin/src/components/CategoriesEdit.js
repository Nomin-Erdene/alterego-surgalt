import { useState } from "react";
import { toast } from "react-toastify";
import mutator from "../utils/mutator";

export default function CategoriesEdit({ category, onComplete, onClose }) {
    const [name, setName] = useState(category.name);
    const [slug, setSlug] = useState(category.slug);

    const [loading, setLoading] = useState(false);

    function submit() {
        setLoading(true);

        mutator(`categories/${category._id}`, { name, slug }, "PUT").then(() => {
            onComplete();
            toast("Амжилттай заслаа");
        });
    }

    return (
        <div className="modal is-active">
            <div className="modal-background"></div>
            <div className="modal-content" style={{ maxWidth: 400 }}>
                <div className="box">
                    <h2 className="title is-3 has-text-centered">Ангилал засах</h2>

                    <div className="field">
                        <label className="label">Нэр</label>
                        <div className="control">
                            <input disabled={loading} value={name} onChange={(e) => setName(e.target.value)} className="input" type="text" placeholder="Ангиллын нэр..." />
                        </div>
                    </div>

                    <div className="field">
                        <label className="label">Холбооc</label>
                        <div className="control">
                            <input disabled={loading} value={slug} onChange={(e) => setSlug(e.target.value)} className="input" type="text" placeholder="Холбооc..." />
                        </div>
                    </div>

                    <hr />

                    <div className="buttons">
                        <button disabled={loading} className="button" onClick={onClose} style={{ flex: 1 }}>
                            Болих
                        </button>
                        <button disabled={loading} className={`button is-link ${loading ? "is-loading" : ""}`} style={{ flex: 1 }} onClick={submit}>
                            Хадгалах
                        </button>
                    </div>
                </div>
            </div>
            <button disabled={loading} className="modal-close is-large" aria-label="close" onClick={onClose}></button>
        </div>
    );
}
