import { useState } from "react";

export default function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    function submit() {
        setLoading(true);

        fetch("http://localhost:8080/users/login", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ username, password }),
        })
            .then((res) => res.json())
            .then((data) => {
                setLoading(false);
                if (data.status === "login") {
                    localStorage.setItem("token", data.token);
                    window.location.reload();
                } else {
                    setError(true);
                }
            });
    }

    return (
        <div>
            <div className="container mt-6" style={{ maxWidth: 300 }}>
                <h1 className="title has-text-centered">Нэвтрэх</h1>

                {error && <div className="notification is-danger">Буруу байна</div>}

                <div className="field">
                    <label className="label">Хэрэглэгчийн нэр</label>
                    <div className="control">
                        <input disabled={loading} value={username} onChange={(e) => setUsername(e.target.value)} className="input" type="text" />
                    </div>
                </div>

                <div className="field">
                    <label className="label">Нууц үг</label>
                    <div className="control">
                        <input disabled={loading} value={password} onChange={(e) => setPassword(e.target.value)} className="input" type="password" />
                    </div>
                </div>

                <hr />

                <button className="button is-link is-fullwidth" onClick={submit}>
                    Нэвтрэх
                </button>
            </div>
        </div>
    );
}
