import { toast } from "react-toastify";

export default function mutator(path, body, method = "POST") {
    return fetch(`http://localhost:8080/${path}`, {
        method: method,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            token: localStorage.getItem("token"),
        },
        body: JSON.stringify(body),
    })
        .then((res) => {
            if (res.ok) {
                return res.json();
            } else {
                toast.error(`Алдаа гарлаа (${res.status})`);
                throw res.status;
            }
        })
        .then((data) => data);
}
