var express = require("express");
var app = express();
var router = express.Router();
const mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");

const userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
});
const User = mongoose.model("User", userSchema);

router.post("/signup", async function (req, res) {
    const { username, password } = req.body;

    var hash = bcrypt.hashSync(password);

    User.create({ username, password: hash }).then((newUser) => {
        res.json(newUser);
    });
});

router.post("/login", async function (req, res) {
    const { username, password } = req.body;

    const user = await User.findOne({ username });

    if (user) {
        bcrypt.compare(password, user.password, function (err, match) {
            if (match) {
                const token = jwt.sign({ userId: user._id }, "nuuts_text");
                res.json({ status: "login", token: token });
            } else {
                res.json({ status: "password.not.match" });
            }
        });
    } else {
        res.json({ status: "not.found" });
    }
});

module.exports = router;
