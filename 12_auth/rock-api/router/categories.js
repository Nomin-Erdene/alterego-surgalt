var express = require("express");
var app = express();
var router = express.Router();
const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
    name: { type: String, required: true },
    slug: { type: String, required: true, unique: true },
});

const Category = mongoose.model("Category", categorySchema);

router.get("/", async function (req, res) {
    const list = await Category.find();
    res.json(list);
});

router.post("/", async function (req, res) {
    const { name, slug } = req.body;
    Category.create({ name, slug }).then((newCategory) => {
        res.json(newCategory);
    });
});

router.delete("/:id", async function (req, res) {
    const { id } = req.params;
    const deleted = await Category.deleteOne({ _id: id });
    res.json(deleted);
});

router.put("/:id", async function (req, res) {
    const { id } = req.params;
    const { name, slug } = req.body;
    const updated = await Category.updateOne({ _id: id }, { name, slug });
    res.json(updated);
});

router.get("/test", function (req, res) {
    const newCategory = new Category({ name: "Улс төр", slug: "politics" });
    newCategory.save().then(() => {
        res.json(newCategory);
    });
});

module.exports = router;
