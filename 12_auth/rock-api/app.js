const express = require("express");
const cors = require("cors");
const app = express();
const port = 8080;
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

mongoose.connect("mongodb+srv://admin:6CnW9NxWknTfA0hq@cluster0.tprtt.mongodb.net/rockDB?retryWrites=true&w=majority");

app.use(cors());

const jsonParser = bodyParser.json();

app.use(jsonParser);

app.get("/", (req, res) => {
    res.send(":)");
});

var jwt = require("jsonwebtoken");

function isAuth(req, res, next) {
    const token = req.header("token");
    try {
        var decoded = jwt.verify(token, "nuuts_text");
        req.userId = decoded.userId;
        next();
    } catch (err) {
        res.sendStatus(401);
    }
}

app.get("/me", isAuth, function (req, res) {
    res.json({ userId: req.userId });
});

const categoriesRouter = require("./router/categories");
app.use("/categories", isAuth, categoriesRouter);

const usersRouter = require("./router/users");
app.use("/users", usersRouter);

app.listen(port, () => {
    console.log(`---------------------------`);
    console.log(`started at ${new Date()}`);
    console.log(`---------------------------`);
});
