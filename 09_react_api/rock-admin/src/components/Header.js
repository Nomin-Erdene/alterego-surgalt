export default function Header() {
    return (
        <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <a className="navbar-item" href="https://bulma.io">
                    Админ{" "}
                </a>

                <a href="https://bulma.io" role="button" className="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                    <a href="https://bulma.io" className="navbar-item">
                        Ангилал
                    </a>
                    <a href="https://bulma.io" className="navbar-item">
                        Мэдээ
                    </a>
                    <a href="https://bulma.io" className="navbar-item">
                        Хэрэглэгч
                    </a>
                    <a href="https://bulma.io" className="navbar-item">
                        Бараа
                    </a>
                </div>

                <div className="navbar-end">
                    <div className="navbar-item">
                        <div className="buttons">
                            <a href="https://bulma.io" className="button is-light">
                                Гарах
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}
