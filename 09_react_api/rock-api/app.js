const express = require("express");
const cors = require("cors");
const app = express();
const port = 8080;
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

mongoose.connect("mongodb+srv://admin:6CnW9NxWknTfA0hq@cluster0.tprtt.mongodb.net/rockDB?retryWrites=true&w=majority");

app.use(cors());

const jsonParser = bodyParser.json();

app.use(jsonParser);

app.get("/", (req, res) => {
    res.send(":)");
});

const categoriesRouter = require("./router/categories");
app.use("/categories", categoriesRouter);

app.listen(port, () => {
    console.log(`---------------------------`);
    console.log(`started at ${new Date()}`);
    console.log(`---------------------------`);
});
