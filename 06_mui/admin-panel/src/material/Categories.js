import { Paper, Button, ButtonGroup, Container, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { useEffect, useState } from "react";

// const categories = [
//     { id: 1, name: "Улс төр", slug: "politics" },
//     { id: 2, name: "Нийгэм", slug: "society" },
//     { id: 3, name: "Cпорт", slug: "sport" },
// ];

export default function Categories() {
    const [categories, setCategories] = useState();

    useEffect(() => {
        fetch("http://localhost:8080/api/categories")
            .then((response) => response.json())
            .then((data) => setCategories(data));
    }, []);

    if (!categories) return "Уншиж байна...";
    if (categories.length === 0) return "Хоосон байна.";

    return (
        <Container maxWidth="sm">
            <Typography variant="h3" component="div" gutterBottom>
                Ангилал
            </Typography>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Нэр</TableCell>
                            <TableCell align="right">Холбоос</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {categories.map((category) => (
                            <TableRow key={category.id}>
                                <TableCell component="th" scope="row">
                                    {category.name}
                                </TableCell>
                                <TableCell align="right">{category.slug}</TableCell>
                                <TableCell>
                                    <ButtonGroup variant="outlined" aria-label="outlined button group">
                                        <Button>Засах</Button>
                                        <Button>Устгах</Button>
                                    </ButtonGroup>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
}
