import "bulma/css/bulma.min.css";
import CategoryEdit from "./CategoryEdit";
import { useState } from "react";

function BulmaApp() {
    const [editing, setEditing] = useState(false);

    function hide() {
        setEditing(false);
    }

    return (
        <div>
            <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
                <div className="navbar-brand">
                    <a className="navbar-item" href="https://bulma.io">
                        Админ{" "}
                    </a>

                    <a href="https://bulma.io" role="button" className="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div id="navbarBasicExample" className="navbar-menu">
                    <div className="navbar-start">
                        <a href="https://bulma.io" className="navbar-item">
                            Ангилал
                        </a>
                        <a href="https://bulma.io" className="navbar-item">
                            Мэдээ
                        </a>
                        <a href="https://bulma.io" className="navbar-item">
                            Хэрэглэгч
                        </a>
                        <a href="https://bulma.io" className="navbar-item">
                            Бараа
                        </a>
                    </div>

                    <div className="navbar-end">
                        <div className="navbar-item">
                            <div className="buttons">
                                <a href="https://bulma.io" className="button is-light">
                                    Гарах
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <div className="container mt-6" style={{ maxWidth: 700 }}>
                <div className="is-flex is-justify-content-space-between">
                    <h1 className="title is-2">Ангилал</h1>

                    <button className="button is-success" onClick={() => setEditing(true)}>
                        Шинэ
                    </button>
                </div>

                <table className="table is-fullwidth">
                    <thead>
                        <tr>
                            <th>Нэр</th>
                            <th>Холбоос</th>
                            <th style={{ width: 1 }}>Тохиргоо</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <strong>Улс төр</strong>
                            </td>
                            <td>
                                <small className="has-text-grey">https://dev.to/t/politics</small>
                            </td>
                            <td>
                                <div className="buttons" style={{ flexWrap: "nowrap" }}>
                                    <button className="button is-small is-info is-light">Засах</button>
                                    <button className="button is-small is-danger is-light">Устгах</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Нийгэм</strong>
                            </td>
                            <td>
                                <small className="has-text-grey">https://dev.to/t/society</small>
                            </td>
                            <td>
                                <div className="buttons" style={{ flexWrap: "nowrap" }}>
                                    <button className="button is-small is-info is-light">Засах</button>
                                    <button className="button is-small is-danger is-light">Устгах</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Cпорт</strong>
                            </td>
                            <td>
                                <small className="has-text-grey"> https://dev.to/t/sport </small>
                            </td>
                            <td>
                                <div className="buttons" style={{ flexWrap: "nowrap" }}>
                                    <button className="button is-small is-info is-light">Засах</button>
                                    <button className="button is-small is-danger is-light">Устгах</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                {editing && <CategoryEdit onClose={hide} />}

                {/* <ConfirmDelete /> */}
            </div>
        </div>
    );
}

export default BulmaApp;
